/* i2c_interface.h
 * I2C interface for MISA to communicate 
 * with the outside wold
 */

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <inttypes.h>

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "devices.h"


/* I2C bus locations */
#define ADDRESS_BB 0x04
#define ADDRESS_SS1 0x05
#define ADDRESS_SS2 0x06
#define ADDRESS_LED_SS1 0x60
#define ADDRESS_LED_SS2 0x61

/* Brightness levels for LEDs */
#define ON 255
#define TOGGLE 35
#define OFF 0

/* Interrupt pins */
#define INTERRUPT_PIN 2

/* Interrupt issued */
// static volatile int interrupt;
static volatile int interrupt;

/* The I2C bus: This is for V2 pi's. For V1 Model B you need i2c-0 */
// static (only 1 copy allowed for the entire program)
static const char* devName = "/dev/i2c-1";
static const uint8_t PWM_ADDR_SS1[] = {2, 3, 4, 5, 17, 16, 6, 7, 15, 14, 9, 8, 10, 11, 12, 13 };
static const uint8_t PWM_ADDR_SS2[] = {2, 3, 4, 5, 0x11, 0x10, 0xA, 7, 0xF, 0xD, 0xB, 9, 0xE, 0xC, 6, 8};

// I2C definitions
int i2c_initialize();
int i2c_close(int);
int i2c_change_bus(int, int);

// read functions
int i2c_read_bus(int, struct BreakBoard*, struct StepSeq* s);
int i2c_read_bb(int, uint16_t*);

// send/write functions
int i2c_send_bytes(int, size_t, uint16_t*);
int i2c_send_byte(int, uint16_t*);
int i2c_send_bpm_to_bb(int, uint8_t);
int i2c_send_to_ss(int, struct StepSeq*, uint16_t*);
int i2c_loop(int);
void i2c_upgrade_sound_state(int, struct BreakBoard*, struct StepSeq*);

//static void interrupt_function(void);

static const uint8_t led_32array[2 * N_BUTTONS][2] = {{0, ADDRESS_LED_SS1}, {1, ADDRESS_LED_SS1}, {2, ADDRESS_LED_SS1}, {3, ADDRESS_LED_SS1},
        {0, ADDRESS_LED_SS2}, {1, ADDRESS_LED_SS2}, {2, ADDRESS_LED_SS2}, {3, ADDRESS_LED_SS2},
        {4, ADDRESS_LED_SS1}, {5, ADDRESS_LED_SS1}, {6, ADDRESS_LED_SS1}, {7, ADDRESS_LED_SS1},
        {4, ADDRESS_LED_SS2}, {5, ADDRESS_LED_SS2}, {6, ADDRESS_LED_SS2}, {7, ADDRESS_LED_SS2},
        {8, ADDRESS_LED_SS1}, {9, ADDRESS_LED_SS1}, {10, ADDRESS_LED_SS1}, {11, ADDRESS_LED_SS1},
        {8, ADDRESS_LED_SS2}, {9, ADDRESS_LED_SS2}, {10, ADDRESS_LED_SS2}, {11, ADDRESS_LED_SS2},
        {12, ADDRESS_LED_SS1}, {13, ADDRESS_LED_SS1}, {14, ADDRESS_LED_SS1}, {15, ADDRESS_LED_SS1},
        {12, ADDRESS_LED_SS2}, {13, ADDRESS_LED_SS2}, {14, ADDRESS_LED_SS2}, {15, ADDRESS_LED_SS2}
    };

static const uint8_t pd_32array[2][N_BUTTONS] = {{0, 1, 2, 3, 8, 9, 10, 11, 16, 17, 18, 19, 24, 25, 26, 27},
        {4, 5, 6, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28, 29, 30, 31}
    };