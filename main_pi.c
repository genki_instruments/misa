#include "i2c_interface.h"
#include <wiringPi.h>
#include <math.h>
#include "portaudio.h"
#include "z_libpd.h"

#define NUM_SECONDS   (5)
#define SAMPLE_RATE   (44100)
#define FRAMES_PER_BUFFER  (1024)

#ifndef M_PI
#define M_PI  (3.14159265)
#endif

#define TABLE_SIZE   (800)

typedef struct
{
    float sine[TABLE_SIZE];
    int left_phase;
    int right_phase;
    char message[20];
} paTestData;

uint8_t next_beat;

static void interrupt_function(void) {
    interrupt = 1;
}

/* This routine will be called by the PortAudio engine when audio is needed.
** It may called at interrupt level on some machines so don't do anything
** that could mess up the system like calling malloc() or free().
*/
static int pd_callback( const void *inputBuffer, void *outputBuffer,
                        unsigned long framesPerBuffer,
                        const PaStreamCallbackTimeInfo* timeInfo,
                        PaStreamCallbackFlags statusFlags,
                        void *userData )
{
    // paTestData *data = (paTestData*)userData;
    float *out = (float*)outputBuffer;
    unsigned long i;

    (void) timeInfo; /* Prevent unused variable warnings. */
    (void) statusFlags;
    (void) inputBuffer;

    float inbuf[64], outbuf[128];  // one input channel, two output channels

    for ( i = 0; i < framesPerBuffer / 64; i++ ) {
        // generate new ticks every 128 bytes (2x)
        libpd_process_float(1, inbuf, outbuf);

        int j = 0;
        for (j = 0; j < 64 * 2; j += 2) {
            *out++ = (float)outbuf[j];  /* left */
            *out++ = (float)outbuf[j + 1]; /* right */
        }
    }

    return paContinue;
}

/*
 * This routine is called by portaudio when playback is done.
 */
static void pd_stream_finished( void* userData )
{
    paTestData *data = (paTestData *) userData;
    printf( "Stream Completed: %s\n", data->message );
}

/*
 * pdfloat is a function that is asynchronously interrupted by pd.
 *   it is used to get current beat index.
**/
void pd_floathook(const char *source, float f) {
    //printf("(%s): %d\n", source, (uint8_t)f);
    next_beat = (uint8_t) f;
}

void pd_send_bpm(uint16_t new_bpm) {
    libpd_float("bpm", new_bpm);
}


void pd_send_toggle(SOUNDS sound, uint8_t button) {
    char lib_tmp[3];
    printf("PD: TOGGLING %d\n", button);
    switch (sound) {
    case BD:
        sprintf(lib_tmp, "b%d", button);
        libpd_bang(lib_tmp);
        break;
    case SN:
        sprintf(lib_tmp, "c%d", button);
        libpd_bang(lib_tmp);
        break;
    case HH:
        sprintf(lib_tmp, "d%d", button);
        libpd_bang(lib_tmp);
        break;
    case COW:
        sprintf(lib_tmp, "e%d", button);
        libpd_bang(lib_tmp);
        break;
    }
}

void pd_reset() {
    int i, j;
    char tmp[3];
    for (i = 0; i < 2 * N_BUTTONS; i++) {
        sprintf(tmp, "b%d", i);
        libpd_float(tmp, 0);
        sprintf(tmp, "c%d", i);
        libpd_float(tmp, 0);
        sprintf(tmp, "d%d", i);
        libpd_float(tmp, 0);
        sprintf(tmp, "e%d", i);
        libpd_float(tmp, 0);
    }
}

void pd_restart_ss1(struct StepSeq* s) {
    int i, j;
    char tmp[3];
    for (i = 0; i < 2 * N_BUTTONS; i++) {
        if (s->states_ss1[0][i]) {
            sprintf(tmp, "b%d", i);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "b%d", i);
            libpd_float(tmp, 0);
        }

        if (s->states_ss1[1][i]) {
            sprintf(tmp, "c%d", i);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "c%d", i);
            libpd_float(tmp, 0);
        }

        if (s->states_ss1[2][i]) {
            sprintf(tmp, "d%d", i);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "d%d", i);
            libpd_float(tmp, 0);
        }

        if (s->states_ss1[2][i]) {
            sprintf(tmp, "e%d", i);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "e%d", i);
            libpd_float(tmp, 0);
        }
    }
}

void pd_restart_ss2(struct StepSeq* s) {
    int i, j;
    char tmp[3];
    for (i = 0; i < 2 * N_BUTTONS; i++) {
        if (s->states_ss2[0][i]) {
            sprintf(tmp, "b%d", i);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "b%d", i);
            libpd_float(tmp, 0);
        }

        if (s->states_ss2[1][i]) {
            sprintf(tmp, "c%d", i);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "c%d", i);
            libpd_float(tmp, 0);
        }

        if (s->states_ss2[2][i]) {
            sprintf(tmp, "d%d", i);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "d%d", i);
            libpd_float(tmp, 0);
        }

        if (s->states_ss2[2][i]) {
            sprintf(tmp, "e%d", i);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "e%d", i);
            libpd_float(tmp, 0);
        }
    }
}

void pd_map_to_32(struct StepSeq* s) {
    char tmp[3];
    int i;
    printf("ENTERING pd_map_to_32\n");

    for (i = 0; i < N_BUTTONS; ++i) {
        // First chekc SS1
        if (s->states_ss1[0][i] == 1) {
            sprintf(tmp, "b%d", pd_32array[0][i]);
            libpd_float(tmp, 1);

        } else {
            sprintf(tmp, "b%d", pd_32array[0][i]);
            libpd_float(tmp, 0);
        }

        if (s->states_ss1[1][i] == 1) {
            sprintf(tmp, "c%d", pd_32array[0][i]);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "c%d", pd_32array[0][i]);
            libpd_float(tmp, 0);
        }

        if (s->states_ss1[2][i] == 1) {
            sprintf(tmp, "d%d", pd_32array[0][i]);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "d%d", pd_32array[0][i]);
            libpd_float(tmp, 0);
        }

        if (s->states_ss1[3][i] == 1) {
            sprintf(tmp, "e%d", pd_32array[0][i]);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "e%d", pd_32array[0][i]);
            libpd_float(tmp, 0);
        }

        if (s->states_ss2[0][i] == 1) {
            sprintf(tmp, "b%d", pd_32array[1][i]);
            libpd_float(tmp, 1);

        } else {
            sprintf(tmp, "b%d", pd_32array[1][i]);
            libpd_float(tmp, 0);
        }

        if (s->states_ss2[1][i] == 1) {
            sprintf(tmp, "c%d", pd_32array[1][i]);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "c%d", pd_32array[1][i]);
            libpd_float(tmp, 0);
        }

        if (s->states_ss2[2][i] == 1) {
            sprintf(tmp, "d%d", pd_32array[1][i]);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "d%d", pd_32array[1][i]);
            libpd_float(tmp, 0);
        }

        if (s->states_ss2[3][i] == 1) {
            sprintf(tmp, "e%d", pd_32array[1][i]);
            libpd_float(tmp, 1);
        } else {
            sprintf(tmp, "e%d", pd_32array[1][i]);
            libpd_float(tmp, 0);
        }



    }

}

void ss_copy_states(struct StepSeq* s, uint8_t from) {
    int i, j;
    uint8_t tmp[N_SOUNDS][N_BUTTONS];

    if (from == SS1_ID) {
        for (j = 0; j < N_SOUNDS; j++) {
            for (i = 0; i < N_BUTTONS; i++) {
                tmp[j][i] = s->states_ss1[j][i];
            }

            for (i = 0; i < 4; i++) {
                s->states_ss1[j][i] = tmp[j][i];
                s->states_ss2[j][i] = tmp[j][i + 4];
            }

            for (i = 4; i < 8; i++) {
                s->states_ss1[j][i] = tmp[j][i + 4];
                s->states_ss2[j][i] = tmp[j][i + 8];
            }

            for (i = 8; i < 16; i++) {
                s->states_ss1[j][i] = s->states_ss1[j][i - 8];
                s->states_ss2[j][i] = s->states_ss2[j][i - 8];
            }
        }

    } else if (from == SS2_ID) {
        for (j = 0; j < N_SOUNDS; j++) {
            for (i = 0; i < N_BUTTONS; i++) {
                tmp[j][i] = s->states_ss2[j][i];
            }

            for (i = 0; i < 4; i++) {
                s->states_ss2[j][i] = tmp[j][i];
                s->states_ss1[j][i] = tmp[j][i + 4];
            }

            for (i = 4; i < 8; i++) {
                s->states_ss2[j][i] = tmp[j][i + 4];
                s->states_ss1[j][i] = tmp[j][i + 8];
            }

            for (i = 8; i < 16; i++) {
                s->states_ss1[j][i] = s->states_ss1[j][i - 8];
                s->states_ss2[j][i] = s->states_ss2[j][i - 8];
            }
        }
    }
}

void ss_copy_states_from_32(struct StepSeq* s, uint8_t to) {
    int i, j;

    if (to == SS1_ID) {
        for (i = 0; i < N_SOUNDS; i++) {
            for (i = 8; i < 12; ++i) {
                s->states_ss1[i][i - 4] = s->states_ss1[i][j];
            }
            for (j = 4; i < 8; ++i) {
                s->states_ss1[i][j] = s->states_ss2[i][j - 4];
            }
            for (i = 12; i < N_BUTTONS; ++i) {
                s->states_ss1[i][j] = s->states_ss2[i][j - 8];
            }

            // WE DON'T WANT TO STORE ANYTHING!
            for (j = 0; i < N_SOUNDS; i++) {
                s->states_ss2[i][j] = 0;
            }
        }
    } else if (to == SS2_ID) {
        for (i = 0; i < N_SOUNDS; i++) {
            for (j = 0; j < 4; j++) {
                s->states_ss2[i][j] = s->states_ss1[i][j + 8];
            }
            for (j = 4; j < 8; j++) {
                s->states_ss2[i][j] = s->states_ss2[i][j + 4];
            }
            for (j = 8; j < 12; j++) {
                s->states_ss2[i][j] = s->states_ss1[i][j + 4];
            }
            // DON'T STROEA ANYHTING
            for (i = 0; i < N_SOUNDS; ++i) {
                    s->states_ss1[i][j] = 0;
            }
        }
    }
}





int main(int argc, char** argv) {

    /* Initialize beat index variable to -1 (Unactive)*/
    next_beat = -1;

    /* Initialize i2c communication and setup */
    int file = i2c_initialize();
    i2c_change_bus(file, ADDRESS_BB);

    if (wiringPiSetup() < 0) {
        fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
        return 1;
    }

    pinMode(INTERRUPT_PIN, INPUT);

    printf("INITIALIZING LIBPD\n");
    PaStreamParameters outputParameters;
    PaStream *stream;
    PaError err;
    paTestData data;

    libpd_set_floathook(pd_floathook);

    libpd_init();
    libpd_init_audio(1, 2, SAMPLE_RATE);
    libpd_bind("position");
    libpd_bind("position32");
    // compute audio
    libpd_openfile("pisampler.pd", ".");

    err = Pa_Initialize();
    if ( err != paNoError ) goto error;

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) {
        fprintf(stderr, "Error: No default output device.\n");
        goto error;
    }
    outputParameters.channelCount = 2;       /* stereo output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;


    err = Pa_OpenStream(
              &stream,
              NULL, /* no input */
              &outputParameters,
              SAMPLE_RATE,
              FRAMES_PER_BUFFER,
              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
              pd_callback,
              &data );
    if ( err != paNoError ) goto error;

    sprintf( data.message, "No Message" );
    err = Pa_SetStreamFinishedCallback( stream, &pd_stream_finished );
    if ( err != paNoError ) goto error;

    err = Pa_StartStream( stream );
    if ( err != paNoError ) goto error;

    /* StepSeq struct for each extr. ss */
    struct StepSeq* s = malloc(SS_SIZE);
    ss_init(s, 1);

    /* BreakBoard struct for the breakout board */
    struct BreakBoard* bb = malloc(BB_SIZE);
    bb->changed_parameter = -1;
    bb->c_sound = 0;
    bb->c_state = NONE;
    uint8_t isplaying = 0;

    /* initialize bpm for pd and all ext. devices */
    pd_send_bpm(INITIAL_TEMPO);
    i2c_send_bpm_to_bb(file, INITIAL_TEMPO);

    // START SEQUENZER
    // libpd_bang("play");

    libpd_float("l16seq", 1);

    uint8_t offset_32 = 0;


    /* Main loop */
    while (1) {
        uint8_t a = digitalRead(INTERRUPT_PIN);

        // First we check if an interrupt is present.
        if (a == 1) {
            printf("INTERRUPTED!\n");
            i2c_read_bus(file, bb, s);
            int i;
            switch (bb->changed_parameter) {
            case CHANGE_BPM:
                if (bb->bpm < MAX_BPM) pd_send_bpm(bb->bpm);
                break;

            case CHANGE_SOUND:
                i2c_upgrade_sound_state(file, bb, s);
                break;

            case CHANGE_STATE:
                switch (bb->c_state) {
                case NONE:
                    pd_reset();
                    //libpd_float("pause", 1);
                    libpd_bang("stop");
                    libpd_float("32seq", 0);
                    libpd_float("l16seq", 0);
                    libpd_float("r16seq", 0);
                    break;

                case SS1:
                    libpd_float("32seq", 0);
                    libpd_float("l16seq", 1);
                    ss_copy_states_from_32(s, SS1_ID);

                    printf("s->states_ss1[0][i]: ");
                    for (i = 0; i < N_BUTTONS; ++i) {
                        printf("%d ", s->states_ss1[0][i]);
                    }
                    printf("\n");

                    pd_restart_ss1(s);
                    i2c_upgrade_sound_state(file, bb, s);

                    break;

                case SS2:
                    libpd_float("32seq", 0);
                    libpd_float("l16seq", 1);
                    libpd_float("r16seq", 1);
                    ss_copy_states_from_32(s, SS2_ID);

                    pd_restart_ss2(s);
                    i2c_upgrade_sound_state(file, bb, s);
                    break;

                case SS_BOTH:
                    libpd_float("r16seq", 0);
                    libpd_float("l16seq", 0);
                    libpd_float("32seq", 1);

                    ss_copy_states(s, bb->toggled_button_ss_id);

                    
                    printf("s->states_ss1[0][i]: ");
                    for (i = 0; i < N_BUTTONS; ++i) {
                        printf("%d ", s->states_ss1[0][i]);
                    }
                    printf("\n");
                    printf("s->states_ss2[0][i]: ");

                    for (i = 0; i < N_BUTTONS; ++i) {
                        printf("%d ", s->states_ss2[0][i]);
                    }
                    printf("\n");

                    pd_map_to_32(s);
                    i2c_upgrade_sound_state(file, bb, s);
                    break;
                }

                break;

            case CHANGE_PLAY_PAUSE:
                if (isplaying) {
                    libpd_bang("pause");
                    isplaying = 0;
                } else {
                    libpd_bang("play");
                    isplaying = 1;
                }

                break;

            default:    // CHECK IF BUTTONS HAVE BEEN TOGGLED
                if (bb->toggled_button_ss_id == SS1_ID) {
                    if (bb->c_state == SS_BOTH) {
                        pd_send_toggle(bb->c_sound, pd_32array[0][bb->changed_parameter]);
                    } else {
                        pd_send_toggle(bb->c_sound, bb->changed_parameter);
                    }

                    i2c_change_bus(file, ADDRESS_LED_SS1);
                    if (s->states_ss1[bb->c_sound][bb->changed_parameter] == 1) {
                        uint8_t tmp[2] = {PWM_ADDR_SS1[bb->changed_parameter], TOGGLE};
                        write(file, tmp, 2);
                    } else {
                        uint8_t tmp[2] = {PWM_ADDR_SS1[bb->changed_parameter], OFF};
                        write(file, tmp, 2);
                    }
                } else if (bb->toggled_button_ss_id == SS2_ID) {
                    // ATHUGA EF 32!!
                    if (bb->c_state == SS_BOTH) {
                        pd_send_toggle(bb->c_sound, pd_32array[1][bb->changed_parameter]);
                    } else {
                        pd_send_toggle(bb->c_sound, bb->changed_parameter + N_BUTTONS);
                    }

                    i2c_change_bus(file, ADDRESS_LED_SS2);
                    if (s->states_ss2[bb->c_sound][bb->changed_parameter] == 1) {
                        uint8_t tmp[2] = {PWM_ADDR_SS2[bb->changed_parameter], TOGGLE};
                        write(file, tmp, 2);
                    } else {
                        uint8_t tmp[2] = {PWM_ADDR_SS2[bb->changed_parameter], OFF};
                        write(file, tmp, 2);
                    }
                }

            }
            bb->changed_parameter = -1;
            bb->toggled_button_ss_id = -1;
        }

        if (bb->c_state == SS_BOTH) {
            if (next_beat >= 0 && next_beat < N_BUTTONS * 2) {
                /*
                if (next_beat == 0) offset_32 = 0;
                if (next_beat == 4 || next_beat == 12 || next_beat == 20 || next_beat == 28) {
                    offset_32 += 4;
                }*/

                //printf("NEXT BEAT: %u, %u\n", next_beat, led_32array[next_beat][0]);

                i2c_change_bus(file, led_32array[next_beat][1]);
                if (led_32array[next_beat][1] == ADDRESS_LED_SS1) {
                    uint8_t tmp[2] = {PWM_ADDR_SS1[led_32array[next_beat][0]], ON};
                    write(file, tmp, 2);
                } else if (led_32array[next_beat][1] == ADDRESS_LED_SS2) {
                    uint8_t tmp[2] = {PWM_ADDR_SS2[led_32array[next_beat][0]], ON};
                    write(file, tmp, 2);
                }

                if (next_beat == 0) {
                    i2c_change_bus(file, led_32array[2 * N_BUTTONS - 1][1]);

                    if (s->states_ss2[bb->c_sound][ led_32array[2 * N_BUTTONS - 1][0] ] == 1) {
                        uint8_t tmp[2] = {PWM_ADDR_SS2[led_32array[2 * N_BUTTONS - 1][0]], TOGGLE};
                        write(file, tmp, 2);
                    } else {
                        uint8_t tmp[2] = {PWM_ADDR_SS2[led_32array[2 * N_BUTTONS - 1][0]], OFF};
                        write(file, tmp, 2);
                    }

                } else {
                    i2c_change_bus(file, led_32array[next_beat - 1][1]);

                    if (led_32array[next_beat - 1][1] == ADDRESS_LED_SS1) {
                        if (s->states_ss1[bb->c_sound][led_32array[next_beat - 1][0]] == 1) {
                            uint8_t tmp[2] = {PWM_ADDR_SS1[led_32array[next_beat - 1][0]], TOGGLE};
                            write(file, tmp, 2);
                        } else {
                            uint8_t tmp[2] = {PWM_ADDR_SS1[led_32array[next_beat - 1][0]], OFF};
                            write(file, tmp, 2);
                        }

                    } else if (led_32array[next_beat - 1][1] == ADDRESS_LED_SS2) {
                        if (s->states_ss2[bb->c_sound][led_32array[next_beat - 1][0]] == 1) {
                            uint8_t tmp[2] = {PWM_ADDR_SS2[led_32array[next_beat - 1][0]], TOGGLE};
                            write(file, tmp, 2);
                        } else {
                            uint8_t tmp[2] = {PWM_ADDR_SS2[led_32array[next_beat - 1][0]], OFF};
                            write(file, tmp, 2);
                        }
                    }
                }
            }
        } else if (bb->c_state == SS1) {
            i2c_change_bus(file, ADDRESS_LED_SS1);

            if (next_beat >= 0 && (next_beat) < N_BUTTONS) {
                printf("SS1, next_beat: %d\n", next_beat);

                if ((next_beat) == 0) {

                    uint8_t tmp[2] = {PWM_ADDR_SS1[next_beat], ON};
                    write(file, tmp, 2);

                    if (s->states_ss1[bb->c_sound][N_BUTTONS - 1] == 1) {
                        uint8_t tmp[2] = {PWM_ADDR_SS1[N_BUTTONS - 1], TOGGLE};
                        write(file, tmp, 2);
                    } else {
                        uint8_t tmp[2] = {PWM_ADDR_SS1[N_BUTTONS - 1], OFF};
                        write(file, tmp, 2);
                    }

                } else {

                    uint8_t tmp[2] = {PWM_ADDR_SS1[next_beat], ON};
                    write(file, tmp, 2);

                    if (s->states_ss1[bb->c_sound][next_beat - 1] == 1) {
                        uint8_t tmp[2] = {PWM_ADDR_SS1[next_beat - 1], TOGGLE};
                        write(file, tmp, 2);
                    } else {
                        uint8_t tmp[2] = {PWM_ADDR_SS1[next_beat - 1], OFF};
                        write(file, tmp, 2);
                    }


                }

            }
        } else if (bb->c_state == SS2) {

            i2c_change_bus(file, ADDRESS_LED_SS2);

            if (next_beat >= 0 && (next_beat) < N_BUTTONS) {
                printf("SS2, next_beat: %d \n", next_beat);

                if ((next_beat) == 0) {

                    uint8_t tmp[2] = {PWM_ADDR_SS2[next_beat], ON};
                    write(file, tmp, 2);

                    if (s->states_ss2[bb->c_sound][N_BUTTONS - 1] == 1) {
                        uint8_t tmp[2] = {PWM_ADDR_SS2[N_BUTTONS - 1], TOGGLE};
                        write(file, tmp, 2);
                    } else {
                        uint8_t tmp[2] = {PWM_ADDR_SS2[N_BUTTONS - 1], OFF};
                        write(file, tmp, 2);
                    }

                } else {

                    uint8_t tmp[2] = {PWM_ADDR_SS2[next_beat], ON};
                    write(file, tmp, 2);

                    if (s->states_ss2[bb->c_sound][next_beat - 1] == 1) {
                        uint8_t tmp[2] = {PWM_ADDR_SS2[next_beat - 1], TOGGLE};
                        write(file, tmp, 2);
                    } else {
                        uint8_t tmp[2] = {PWM_ADDR_SS2[next_beat - 1], OFF};
                        write(file, tmp, 2);
                    }


                }

            }
        }
        next_beat = -1;
        if (next_beat < 255 && next_beat >= 0) printf("%d\n", next_beat);
        Pa_Sleep(1);
    }


    err = Pa_StopStream( stream );
    if ( err != paNoError ) goto error;

    err = Pa_CloseStream( stream );
    if ( err != paNoError ) goto error;

    Pa_Terminate();
    printf("Test finished.\n");
    free(s);

    return err;
error:
    Pa_Terminate();
    fprintf( stderr, "An error occured while using the portaudio stream\n" );
    fprintf( stderr, "Error number: %d\n", err );
    fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( err ) );
    return err;
    i2c_close(file);
    return (EXIT_SUCCESS);
}