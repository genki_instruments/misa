#include "i2c_interface.h"

/* i2c_initilize
 *   read a byte from i2c bus indicated by
 */
int i2c_initialize() {
    int file;
    if ((file = open(devName, O_RDWR)) < 0) {
        fprintf(stderr, "I2C: Failed to access %s\n", devName);
        exit(1);
    }
    return file;
}

int i2c_close(int file) {
    if (close(file)) {
        fprintf(stderr, "I2C: Failed to close %s\n", devName);
        exit(1);
    }

    return (EXIT_SUCCESS);
}

/* i2c_change_bus
 *   change the bus i2c communications operate over bus with address address
 */
int i2c_change_bus(int file, int address) {
    //printf("I2C: acquiring buss to 0x%x\n", address);
    if (ioctl(file, I2C_SLAVE, address) < 0) {
        fprintf(stderr, "I2C: Failed to acquire bus access/talk to slave 0x%x\n", address);
        exit(1);
    }

    return (EXIT_SUCCESS);
}


int i2c_read_bus(int file, struct BreakBoard* bb, struct StepSeq* s) {
    i2c_change_bus(file, ADDRESS_BB);
    char tmp[3];
    if (read(file, tmp, 3) != 3) {
        // Error writing to i2c slave
        return -1;
    }

    printf("RECEIVED, %d, %d, %d\n", tmp[0], tmp[1], tmp[2]);
    uint16_t tmp_bmp;
    uint8_t button, id, on_off;

    switch ((uint8_t) tmp[0]) {
    case 1: // bpm
        tmp_bmp = ((uint16_t) tmp[2] << 8) | (uint16_t) tmp[1];
        if (tmp_bmp < MAX_BPM) {
            bb->bpm = tmp_bmp;
            bb->changed_parameter = CHANGE_BPM;
        } else {
            bb->changed_parameter = -1;
        }
        break;

    case 2: // button toggle
        button = tmp[1] & 0x0F;
        id = (tmp[1] >> 4) & 0x07;
        on_off = (tmp[1] >> 7);

        if (id == SS1_ID) {
            bb->changed_parameter = button;
            bb->toggled_button_ss_id = id;
            s->states_ss1[bb->c_sound][button] = !s->states_ss1[bb->c_sound][button];
            printf("NEW BUTTON LOCATION RECEIVED, id: %d, button: %d, on/off: %d\n",
                   id, bb->changed_parameter, s->states_ss1[bb->c_sound][button]);
        } else if (id == SS2_ID) {
            bb->changed_parameter = button; // ADDED FOR BREVITY
            bb->toggled_button_ss_id = id;
            s->states_ss2[bb->c_sound][button] = !s->states_ss2[bb->c_sound][button];
            printf("NEW BUTTON LOCATION RECEIVED, id: %d, button: %d, on/off: %d\n",
                   id, bb->changed_parameter, s->states_ss2[bb->c_sound][button]);
        } else {
            bb->changed_parameter = -1;
        }
        break;

    case 3: // current sound
        if ((uint8_t) tmp[1] == 1) {
            bb->c_sound = (bb->c_sound + 1) % 4;
            //printf("CURRENT STATE: %d\n", bb->c_sound);
            bb->changed_parameter = CHANGE_SOUND;
        } else if ((uint8_t) tmp[1] == 2) {
            bb->c_sound = (bb->c_sound + 3) % 4;
            //printf("CURRENT STATE: %d\n", bb->c_sound);
            bb->changed_parameter = CHANGE_SOUND;
        }
        break;

    case 4: // device attach/detach; WHAT DO WE NEED TO DO HERE??
        printf("I'm ATTACHING/DETACHING SUM SHIT Y'A!, b->c_state: %d\n", bb->c_state);
        bb->changed_parameter = CHANGE_STATE;
        uint8_t cur_id = tmp[1];
        uint8_t cur_on_off = tmp[2];
        switch (bb->c_state) {
        case NONE:
            if (cur_id == SS1_ID && cur_on_off == 1) {
                bb->c_state = SS1;
                printf("JUST ENTERED STATE SS1\n");
            } else if (cur_id == SS2_ID && cur_on_off == 1) {
                bb->c_state = SS2;
                printf("JUST ENTERED STATE SS12\n");
            }
            break;

        case SS1:
            if (cur_id == SS1_ID && cur_on_off == 0) {
                bb->c_state = NONE;
                printf("JUST ENTERED STATE NONE\n");
            } else if (cur_id == SS2_ID && cur_on_off == 1) {
                bb->c_state = SS_BOTH;
                bb->toggled_button_ss_id = SS1_ID;
                printf("JUST ENTERED STATE SS_BOTH\n");
            }
            break;

        case SS2:
            if (cur_id == SS2_ID && cur_on_off == 0) {
                bb->c_state = NONE;
                printf("JUST ENTERED STATE NONE\n");
            } else if (cur_id == SS1_ID && cur_on_off == 1) {
                bb->c_state = SS_BOTH;
                bb->toggled_button_ss_id = SS2_ID;
                printf("JUST ENTERED STATE SS_BOTH\n");
            }
            break;

        case SS_BOTH:
            if (cur_id == SS2_ID && cur_on_off == 0) {
                bb->c_state = SS1;
                printf("JUST ENTERED STATE SS1\n");
            } else if (cur_id == SS1_ID && cur_on_off == 0) {
                bb->c_state = SS2;
                printf("JUST ENTERED STATE SS2\n");
            }
            break;
        }

        break;

    case 5: // play/stop
        bb->changed_parameter = CHANGE_PLAY_PAUSE;
        printf("PLAY/PAUSE!\n");
        break;

    }

return (EXIT_SUCCESS);
}

// WARNING!!! ERROR!!
void i2c_upgrade_sound_state(int file, struct BreakBoard* bb, struct StepSeq* s) {
    printf("UPGRADING SOUND STATE\n");
    int i;
    for (i = 0; i < N_BUTTONS; i++) {
        i2c_change_bus(file, ADDRESS_LED_SS1);
        if (s->states_ss1[bb->c_sound][i] == 1) {
            uint8_t tmp[2] = {PWM_ADDR_SS1[i], TOGGLE};
            write(file, tmp, 2);
        } else {
            uint8_t tmp[2] = {PWM_ADDR_SS1[i], OFF};
            write(file, tmp, 2);
        }
        
        i2c_change_bus(file, ADDRESS_LED_SS2);
        if (s->states_ss2[bb->c_sound][i] == 1) {
            uint8_t tmp[2] = {PWM_ADDR_SS2[i], TOGGLE};
            write(file, tmp, 2);
        } else {
            uint8_t tmp[2] = {PWM_ADDR_SS2[i], OFF};
            write(file, tmp, 2);
        }
    }
}



/* i2send_bytes
 *
 */
int i2c_send_bytes(int file, size_t number_of_bytes, uint16_t* buf) {
    // ATTENTION, WILL GO TO SLEEP!
    usleep(1000);
    // NEED TO TAKE A LOOK AT THIS!
    if (write(file, buf, number_of_bytes) != number_of_bytes) {
        // Error writing to i2c slave
        return -1;
    }
    return (EXIT_SUCCESS);
}

/* i2send_byte */
int i2c_send_byte(int file, uint16_t* buf) {
    return i2c_send_bytes(file, 1, buf);
}

// THIS FUNCTION COULD BE WRONG!
int i2c_send_bpm_to_bb(int file, uint8_t bpm) {
    uint8_t tmp[2] = {bpm, 0};
    i2c_change_bus(file, ADDRESS_BB);
    write(file, tmp, 2);
    return -1;
}

int i2c_send_to_ss(int file, struct StepSeq *s, uint16_t* buf) {
    return -1;
}