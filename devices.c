#include "devices.h"

void ss_init(struct StepSeq* s, uint8_t id) {
    s->ss1_on = 0;
    s->ss2_on = 0;
    int i, j;
    for (j = 0; j < N_SOUNDS; j++) {
        for (i = 0; i < N_BUTTONS; ++i) {
            s->states_ss1[j][i] = 0;
            s->states_ss2[j][i] = 0;
        }
    }
}