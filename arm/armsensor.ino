#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>

#include<Wire.h>
const int MPU=0x68;  // I2C address of the MPU-6050
const int LSM=0x3A; // I2C address of LSM-303D
byte Read    = 0B00000001;
byte Write   = 0B00000000;
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ,MgX,MgY,MgZ;
double angleX,angleY,gyroAngle,GyXrate,GyYrate;
double avAngleX,avAngleY,gyroX,gyroY;
double GyXoffset,GyYoffset;
double AcXoffset,AcYoffset,AcZoffset;
uint32_t timer,timer2;
int count = 0;

/*
This sketch sends a string to a corresponding Arduino
 with nrf24 attached.  It appends a specific value 
 (2 in this case) to the end to signify the end of the
 message.
 */

int msg[1];
RF24 radio(9,10);
const uint64_t pipe = 0xE8E8F0F0E1LL;

void setup(void){
  Wire.begin();
  //WriteRegister(MPU,0x6B,0); //wake up MPU-6050
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
  Wire.beginTransmission(MPU);
  Wire.write(0x1A); //Low pass filter
  Wire.write(0); //Turn on
  Wire.endTransmission(true);
  //WriteRegister(MPU,0x1A,0); //configure LP filter
  /*Wire.beginTransmission(LSM);
  Wire.write(0x20);
  Wire.write(0);
  Wire.endTransmission(true);
  Wire.beginTransmission(LSM);
  Wire.write(0x24);
  Wire.write(8);
  Wire.endTransmission(true);
  Wire.beginTransmission(LSM);
  Wire.write(0x26);
  Wire.write(0);
  Wire.endTransmission(true);*/
  WriteRegister(LSM,0x20,0); //Turn off LSM accelerometer
  WriteRegister(LSM,0x24,8); //Turn on LSM magnetometer
  WriteRegister(LSM,0x26,0);
  Serial.begin(57600);
  radio.begin();
  radio.openWritingPipe(pipe);
  timer = micros();
  
  for(int i=0;i<1000;i++)
  {
    Wire.beginTransmission(MPU);
    Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
    Wire.endTransmission(true);
    Wire.requestFrom(MPU,14,true);  // request a total of 14 registers
    AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
    AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
    GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
  
    /*byte X_H_A = ReadRegister(MPU,0x3B);
    byte X_L_A = ReadRegister(MPU,0x3C);
    byte Y_H_A = ReadRegister(MPU,0x3D);
    byte Y_L_A = ReadRegister(MPU,0x3E);
    byte Z_H_A = ReadRegister(MPU,0x3F);
    byte Z_L_A = ReadRegister(MPU,0x40);
    byte X_H_G = ReadRegister(MPU,0x43);
    byte X_L_G = ReadRegister(MPU,0x44);
    byte Y_H_G = ReadRegister(MPU,0x45);
    byte Y_L_G = ReadRegister(MPU,0x46);
    byte Z_H_G = ReadRegister(MPU,0x47);
    byte Z_L_G = ReadRegister(MPU,0x48);
    AcX=X_H_A<<8|X_L_A;  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
    AcY=Y_H_A<<8|Y_L_A;  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ=Z_H_A<<8|Z_L_A;  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    GyX=X_H_G<<8|X_L_G;  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    GyY=Y_H_G<<8|Y_L_G;  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    GyZ=Z_H_G<<8|Z_L_G;  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)*/
    
    GyXoffset += GyX;
    GyYoffset += GyY;
    AcXoffset += AcX;
    AcYoffset += AcY;
    AcZoffset += AcZ;
  }
  GyXoffset = GyXoffset/1000;
  GyYoffset = GyYoffset/1000;
  AcXoffset = AcXoffset/1000;
  AcYoffset = AcYoffset/1000;
  AcZoffset = AcZoffset/1000;
  
  /*GyXoffset = GyXoffset/4;
  GyYoffset = GyYoffset/4;
  AcXoffset = AcXoffset/8;
  AcYoffset = AcYoffset/8;
  AcZoffset = (16384-AcZoffset)/8;*/
  
  delay(100);
}

void loop(void){
  double dt = (double)(micros()-timer)/1000000;
  timer = micros();
  
  Wire.beginTransmission(MPU);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(true);
  Wire.requestFrom(MPU,14,true);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
  
  /*AcX = AcX-AcXoffset;
  AcY = AcY-AcYoffset;
  AcZ = AcZ-AcZoffset;*/
  
  /*byte X_H_A = ReadRegister(MPU,0x3B);
  byte X_L_A = ReadRegister(MPU,0x3C);
  byte Y_H_A = ReadRegister(MPU,0x3D);
  byte Y_L_A = ReadRegister(MPU,0x3E);
  byte Z_H_A = ReadRegister(MPU,0x3F);
  byte Z_L_A = ReadRegister(MPU,0x40);
  byte X_H_G = ReadRegister(MPU,0x43);
  byte X_L_G = ReadRegister(MPU,0x44);
  byte Y_H_G = ReadRegister(MPU,0x45);
  byte Y_L_G = ReadRegister(MPU,0x46);
  byte Z_H_G = ReadRegister(MPU,0x47);
  byte Z_L_G = ReadRegister(MPU,0x48);
  AcX=X_H_A<<8|X_L_A;  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
  AcY=Y_H_A<<8|Y_L_A;  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Z_H_A<<8|Z_L_A;  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  GyX=X_H_G<<8|X_L_G;  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Y_H_G<<8|Y_L_G;  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Z_H_G<<8|Z_L_G;  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)*/
  
  /*Serial.print("AcX = ");Serial.print(AcX);
  Serial.print(" AcY = ");Serial.print(AcY);
  Serial.print(" AcZ = ");Serial.print(AcZ);
  Serial.print(" GyX = ");Serial.print(GyX);
  Serial.print(" GyY = ");Serial.print(GyY);
  Serial.print(" GyZ = ");Serial.println(GyZ);*/
  
  /*byte X_H_M = ReadRegister(LSM,0x09);
  byte X_L_M = ReadRegister(LSM,0x08);
  byte Y_H_M = ReadRegister(LSM,0x0B);
  byte Y_L_M = ReadRegister(LSM,0x0A);
  byte Z_H_M = ReadRegister(LSM,0x0D);
  byte Z_L_M = ReadRegister(LSM,0x0C);
  
  MgX = X_H_M<<8|X_L_M;
  MgY = Y_H_M<<8|Y_L_M;
  MgZ = Z_H_M<<8|Z_L_M;
  /*Wire.beginTransmission(LSM);
  Wire.write(0x08);
  Wire.endTransmission(true);
  Wire.requestFrom(LSM,6,true);
  byte X_L_M = Wire.read();
  byte X_H_M = Wire.read();
  byte Y_L_M = Wire.read();
  byte Y_H_M = Wire.read();
  byte Z_H_M = Wire.read();
  byte Z_L_M = Wire.read();
  MgX = X_H_M<<8|X_L_M;
  MgY = Y_H_M<<8|Y_L_M;
  MgZ = Z_H_M<<8|Z_L_M;*/
  
  double accX = (AcX)/16384;
  double accY = (ACY)/16384;
  double accZ = (AcZ)/16384;
  
  double roll = atan2(AcY,AcZ)*180/PI;
  double pitch = atan(AcX/AcZ)*180/PI;
  gyroX = (GyX-GyXoffset)/131.0*dt;
  gyroY = (GyY-GyYoffset)/131.0*dt;
  //angleX = 0.93*gyroX + 0.07*roll;
  //angleY = 0.93*gyroY + 0.07*pitch;
  angleX = 0.93*(angleX - gyroX) + 0.07*roll;
  angleY = 0.93*(angleY - gyroY) + 0.07*pitch;
  
  /*Serial.print("AngleX = ");Serial.print((int)angleX);
  Serial.print(" AngleY = ");Serial.print((int)angleY);
  Serial.print(" MgX = ");Serial.print(MgX);
  Serial.print(" MgY = ");Serial.print(MgY);
  Serial.print(" MgZ = ");Serial.println(MgZ);*/
  
  int messageX = angleX;
  int messageY = angleY;
  int messageAX = accX;
  int messageAZ = accZ;
  radio.write(&messageAZ, sizeof(int));
}

byte ReadRegister(int Address, int Register){
byte result = 0;
Wire.beginTransmission((Address | Write) >>1); //slave ID start talking
//ask for info in register
Wire.write(Register);
//complete the send
Wire.endTransmission(0);
//Request 1 byte

Wire.requestFrom((Address | Read) >>1 , 1);
//wait for info
while( Wire.available() == 0);
result = Wire.read();  
//get info
Wire.endTransmission();
return(result);  
}

void WriteRegister( int Address, byte Register, byte Value){
  Wire.beginTransmission((Address | Write) >>1);
  Wire.write(Register);
  Wire.write(Value);
  Wire.endTransmission();
}
