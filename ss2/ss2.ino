/*
/ Arduino code for step sequencer 2
 /
 */

#include <Wire.h>
#include <PinChangeInt.h>
# include <inttypes.h>

#define TWI_FREQ 400000L

// I2C bus addresses
#define LED_DRIVER_ADDRESS             0b1100001  // A0 pulled high, A1-A3 pulled low
#define ALLCALL_ADDRESS                0b1101000
#define RESET_ADDRESS                  0b1101011
#define THIS_ADDRESS                   0x6        // This device's address
#define BB_ADDRESS                     0x4        // BreakoutBoard address

// I2C R/W
#define I2C_READ                       1
#define I2C_WRITE                      0

// Control register (three MSB control auto-increment)
#define NO_AUTO_INCREMENT              0b00000000
#define AUTO_INCREMENT_ALL_REGISTERS   0b10000000
#define AUTO_INCREMENT_BRIGHTNESS      0b10100000
#define AUTO_INCREMENT_CONTROL         0b11000000
#define AUTO_INCREMENT_BRIGHT_CONTROL  0b11100000

// TLC59116 registers
#define TLC59116_GRPPWM                0x12
#define TLC59116_LEDOUT0               0x14

// LED output state for LEDOUT0 to LEDOUT3
#define LED_OUTPUT_OFF                 0b00
#define LED_OUTPUT_GROUP               0b11

// LED brightness parameters
#define ON       255
#define OFF      0
#define TOGGLE   20

// Number of buttons per stepseq
#define N 16
volatile int interupt_id = -1;   // Current button toggle indicator
int buttons[N] = {
  A0, A3, 1, 0, A2, A1, 5, 2, 12, 7, 4, 3, 10, 9, 8, 6
};

// Define LED outputs
int leds[3][16] = {
  {
    1, 2, 4, 8, 32768, 16384, 256, 32, 8192, 2048, 512, 128, 4096, 1024, 16, 64
  }
  , {
    0
  }
  , {
    0
  }
};

void setup() {
  Wire.begin(THIS_ADDRESS); // Enter I2C bus as slave

  // Transmit to the TLC59116
  Wire.beginTransmission(LED_DRIVER_ADDRESS);
  // Send the control register.  All registers will be written to, starting at register 0
  Wire.write(byte(AUTO_INCREMENT_ALL_REGISTERS));
  // Set MODE1: no sub-addressing
  Wire.write(byte(0));
  // Set MODE2: dimming
  Wire.write(byte(0));
  // Set individual brightness control to minimum
  for (int i = 0; i < N; i++)
    Wire.write(byte(0x0));
  // Set GRPPWM: Full brightness
  Wire.write(byte(0xFF));
  // Set GRPFREQ: Not blinking, must be 0
  Wire.write(byte(0));

  // Set LEDs ON for now
  for (int i = 0; i < 4; i++) {
    delay(1);
    Wire.write(byte((uint8_t)255));
  }

  // TLC59116 transmission done
  Wire.endTransmission();
  delay(100);

  // Attach interrupts to button pins and configure pullup resistors on
  for (int i = 0; i < N; i++) {
    pinMode(buttons[i], INPUT_PULLUP);
  }

  // Attach interrupts with buttons
  attachPinChangeInterrupt(buttons[0], button0_intrpt, RISING);
  attachPinChangeInterrupt(buttons[1], button1_intrpt, RISING);
  attachPinChangeInterrupt(buttons[2], button2_intrpt, RISING);
  attachPinChangeInterrupt(buttons[3], button3_intrpt, RISING);
  attachPinChangeInterrupt(buttons[4], button4_intrpt, RISING);
  attachPinChangeInterrupt(buttons[5], button5_intrpt, RISING);
  attachPinChangeInterrupt(buttons[6], button6_intrpt, RISING);
  attachPinChangeInterrupt(buttons[7], button7_intrpt, RISING);
  attachPinChangeInterrupt(buttons[8], button8_intrpt, RISING);
  attachPinChangeInterrupt(buttons[9], button9_intrpt, RISING);
  attachPinChangeInterrupt(buttons[10], button10_intrpt, RISING);
  attachPinChangeInterrupt(buttons[11], button11_intrpt, RISING);
  attachPinChangeInterrupt(buttons[12], button12_intrpt, RISING);
  attachPinChangeInterrupt(buttons[13], button13_intrpt, RISING);
  attachPinChangeInterrupt(buttons[14], button14_intrpt, RISING);    
  attachPinChangeInterrupt(buttons[15], button15_intrpt, RISING);
  
  // Signal BB that I've joined the bus
}


void loop() {
  if (interupt_id >= 0) {
    sendButtonChange(interupt_id);
    interupt_id = -1;
  }
}

// Send button toggle information to breakout board as one byte
// Lower 4 bits represent button ID
// Higher 8 bits represent sequencer ID
void sendButtonChange(int id) {
  char tmp;
  tmp = THIS_ADDRESS << 4;  // Sequencer ID
  tmp = tmp | id;           // Button ID
  tmp = (leds[1][id] << 7) | tmp;
  
  char data[] = {tmp,255};

  Wire.beginTransmission(BB_ADDRESS);
  Wire.write(data,2);
  Wire.endTransmission();
}

// Interrupt Service Routines for buttons
void button0_intrpt() {
  interupt_id = 0;
}
void button1_intrpt() {
  interupt_id = 1;
}
void button2_intrpt() {
  interupt_id = 2;
}
void button3_intrpt() {
  interupt_id = 3;
}
void button4_intrpt() {
  interupt_id = 4;
}
void button5_intrpt() {
  interupt_id = 5;
}
void button6_intrpt() {
  interupt_id = 6;
}
void button7_intrpt() {
  interupt_id = 7;
}
void button8_intrpt() {
  interupt_id = 8;
}
void button9_intrpt() {
  interupt_id = 9;
}
void button10_intrpt() {
  interupt_id = 10;
}
void button11_intrpt() {
  interupt_id = 11;
}
void button12_intrpt() {
  interupt_id = 12;
}
void button13_intrpt() {
  interupt_id = 13;
}
void button14_intrpt() {
  interupt_id = 14;
}
void button15_intrpt() {
  interupt_id = 15;
}


