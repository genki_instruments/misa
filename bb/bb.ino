/* bb.ino
 *   arduino code for MISA breakout board
 */
#include <PinChangeInt.h>
#include <Wire.h>

#define TWI_FREQ 400000L

// i2c address
#define THIS_ADDRESS 0x04
#define SS1_ADDRESS 0x05
#define SS2_ADDRESS 0x06

// Declare input pins
int PIN_A1 = 3;
int PIN_B1 = 4;
int PIN_A2 = 6;
int PIN_B2 = 7;
int PIN_TAP1 = 5;
int PIN_TAP2 = 8;
int LED = 13;
int RPI = A0;

// delay parameters
double totalDelay, d0, d1;
long int_time, int_time2;

// Queue variables to store tap tempo data
const int N = 4;
uint16_t queue[N];
int queue_i = 0;  
int queue_j = 0;
int queue_count = 0;

// Notice that values that get modified inside an interrupt, that I wish to access
// outside the interrupt, are marked "volatile". It tells the compiler not to optimize
// the variable.
volatile uint16_t currentVal = 0;
volatile uint16_t lastVal = 0;
volatile uint16_t bpm = 160;  // Might want to declare a MAX BPM to hinder overflow
volatile uint8_t bpmChanged = 0;

// Running variables and other misc parameters
char stepSequencerData;
int buttonChanged = 0;
int int_flag = 0;
int stateChanged = 0;
int stateChangeRead = 1;
int stepState = 0;
int nDevices = 0;
int SS1_CONNECTED, SS2_CONNECTED, SS1_LAST, SS2_LAST, NEW_DEVICE;
int play_stop = 0;
int play = 0;

// Tap tempo
volatile uint16_t currentState = 0;
volatile unsigned long t_last = 0;
// Variable to keep track of time delay since tap-tempo state was changed last time
volatile unsigned long t_effect_last = 0; 
volatile unsigned long DELTA_MIN = 150;
volatile unsigned long DELTA_MAX = 1500;

// callback for sending data upon request
void sendData() {
  // Set interrupt pin and flag low to indicate that the interrupt has been served
  int_flag = 0;
  digitalWrite(RPI,0);
  
  // We send three bytes upon request
  // First byte identifies the nature of the data included in the last two bytes
  // 1: BPM,  2: Button info from stepseq, 3: Sound change,  
  // 4: Device connected/disconnected,  5: Play/stop
  byte data[] = {0,0,0};
  int N = 3;        // Number of bytes to send
  
  if(bpmChanged) {
    char lo = bpm & 0xFF;
    char hi = bpm >> 8;
    data[0] = 1;          // Command
    data[1] = byte(lo);
    data[2] = byte(hi);
    Wire.write(data,N);
    bpmChanged = 0;            // // Re-initialize state change flag
  }  
  if(buttonChanged) {
    data[0] = 2;          // Command
    data[1] = stepSequencerData;
    Wire.write(data,N);
    buttonChanged = 0;        // Re-initialize state change flag
  }
  if(!stateChangeRead) {
    data[0] = 3;     // Command
    data[1] = stepState;
    Wire.write(data,N);
    stateChangeRead = 1;
  }
  if(NEW_DEVICE > 0) {
    data[0] = 4;      // Command
    if(NEW_DEVICE == SS1_ADDRESS) {
      data[1] = SS1_ADDRESS;
      data[2] = SS1_CONNECTED;
    }
    if(NEW_DEVICE == SS2_ADDRESS) {
      data[1] = SS2_ADDRESS;
      data[2] = SS2_CONNECTED;
    }
    Wire.write(data,N);
    NEW_DEVICE = 0;      // Re-initialize state change flag
  }
  if(play_stop) {
    data[0] = 5;    // Command
    data[1] = play;
    Wire.write(data,N);
    play_stop = 0;      // Re-initialize state change flag
  }
    
}

// Receive data , either from stepseq or RPI
void receiveData(int byteCount) {
  byte data[] = {0,0};
  
  while(Wire.available()) {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }
  
  // Received BPM from PI
  if(data[1] < 255) {
    char tmp;
    tmp = data[0] & 0xFF;
    tmp = (data[1] << 8) | tmp;
    bpm = tmp;
  }
  // Received button change info from stepseq
  // Four lower bits represent button ID
  // Four upper bits represent sequencer ID
  else {
    stepSequencerData = data[0];
    interruptRPi();
    buttonChanged = 1;  
  }
}

// Send interrupt to RPI
void interruptRPi() {
  digitalWrite(RPI, 1);
  int_time = millis();
  int_flag = 1;            // This flag is set to 0 when interrupt has been served
}

uint16_t readInputPins(int pin1, int pin2) {
  int valA = digitalRead(pin1);
  int valB = digitalRead(pin2);

  return 2 * valA + valB;
}

void interruptFunction1() {
  currentVal = readInputPins(PIN_A1,PIN_B1);

  // First we check if turning clockwise and then if turning counter clockwise
  if(currentVal == 3 && lastVal == 1) {
    bpm++;
    bpmChanged = 1; // Er mÃ¶gulega hÃ¦gt aÃ° breyta Ã¾essu Ã¾annig aÃ° ef breyting upp  +/- bpm tha sendanum vid, annars ekki
  } 
  else if(currentVal == 3 && lastVal == 2) {
    bpm--;
    bpmChanged = 1; 
  }

  lastVal = currentVal;
}

// Interrupt function 
void interruptFunction2() {
  currentVal = readInputPins(PIN_A2,PIN_B2);

  // First we check if turning clockwise and then if turning counter clockwise
  if(currentVal == 3 && lastVal == 1) {
    stepState = 1;
    
    stateChanged = 1; // Er mÃ¶gulega hÃ¦gt aÃ° breyta Ã¾essu Ã¾annig aÃ° ef breyting upp  +/- bpm tha sendanum vid, annars ekki
  } 
  else if(currentVal == 3 && lastVal == 2) {
    stepState = 2;
    
    stateChanged = 1;
  }

  lastVal = currentVal;
}

uint16_t weighted_sum(uint16_t queue[]) {
  uint16_t sum = 0;

  for(int i = 0; i < N; i++) {
    sum += queue[i];
  }

  return sum/(queue_count*1.0);
}

void tapTempo() {
  unsigned long t = millis();
  // Very simple check to eliminate debouncing
  if(t - t_effect_last > DELTA_MIN) {
    if((t - t_last > DELTA_MIN) && (t - t_last < DELTA_MAX)) {
      if(currentState == 0) {
        currentState = 1;

        uint16_t current_bpm = 60000/(t - t_last);
        queue_count++;
        queue[queue_j] = current_bpm;
        queue_j = (queue_j + 1) % N;

      } 
      else if(currentState == 1){
        currentState = 2;

        uint16_t current_bpm = 60000/(t - t_last);
        queue_count++;
        queue[queue_j] = current_bpm;
        queue_j = (queue_j + 1) % N;

      } 
      else if(currentState == 2){
        currentState = 3;

        uint16_t current_bpm = 60000/(t - t_last);
        queue[queue_j] = current_bpm;
        queue_j = (queue_j + 1) % N;
        queue_count++;
        bpm = weighted_sum(queue);
        bpmChanged = 1;
      } 
      else if(currentState == 3){
        uint16_t current_bpm = 60000/(t - t_last);
        queue[queue_j] = current_bpm;
        queue_j = (queue_j + 1) % N;
        if(queue_count < N)
          queue_count++;
        bpm = weighted_sum(queue);
        bpmChanged = 1;
      } 
    } 
    else {
      //      Serial.println("ENTERING STATE 0");
      currentState = 0;

      for(int i = 0; i < queue_count; i++) {
        queue[i] = 0;
      }
      queue_count = 0;
    } 

    t_effect_last = millis();
  }

  t_last = t;
}

void playStopInterrupt() {
  play_stop = 1;
  play = !play;
}


void setup() {
  // initialize i2c as slave
  Wire.begin(THIS_ADDRESS);
  Wire.onRequest(sendData);
  Wire.onReceive(receiveData);
  
  SS1_CONNECTED = 0;
  SS2_CONNECTED = 0;

  // Turn pullup resistors on for all pins on encoder
  pinMode(PIN_A1, INPUT_PULLUP);
  pinMode(PIN_B1, INPUT_PULLUP);
  pinMode(PIN_A2, INPUT_PULLUP);
  pinMode(PIN_B2, INPUT_PULLUP);
  attachPinChangeInterrupt(PIN_A1, interruptFunction1, RISING);
  attachPinChangeInterrupt(PIN_B1, interruptFunction1, RISING);
  attachPinChangeInterrupt(PIN_A2, interruptFunction2, RISING);
  attachPinChangeInterrupt(PIN_B2, interruptFunction2, RISING);

  pinMode(PIN_TAP1, INPUT_PULLUP);
  pinMode(PIN_TAP2, INPUT_PULLUP);
  attachPinChangeInterrupt(PIN_TAP1, tapTempo, FALLING);
  attachPinChangeInterrupt(PIN_TAP2, playStopInterrupt, FALLING);
  
  pinMode(LED, OUTPUT);
  pinMode(RPI, OUTPUT);

  digitalWrite(RPI,0);
}

void loop() {
  totalDelay = 1000;
  d0 = millis();
  
  SS1_LAST = SS1_CONNECTED;
  SS2_LAST = SS2_CONNECTED;
  
  d1 = millis();
  
  // Loop while not doing anything important
  while(totalDelay > d1 - d0) {
    d1 = millis();
    
    // Check if BPM change has happened
    if((bpmChanged || buttonChanged) && !int_flag) {      
      interruptRPi();
    }
    
    // Check if state change has happened
    if(stateChanged) {
      interruptRPi();
      stateChangeRead = 0;
      stateChanged = 0;
    }
    
    // Check if play/stop event has happened
    if(play_stop) {
      interruptRPi();
    }
    
    if(millis()-int_time > 100 && int_flag) {
      digitalWrite(RPI,0);
      //interruptRPi();
    }
  }
  
  // Scan I2C bus for devices
  Wire.beginTransmission(SS1_ADDRESS);
  SS1_CONNECTED = !Wire.endTransmission();
  delay(10);
  Wire.beginTransmission(SS2_ADDRESS);
  SS2_CONNECTED = !Wire.endTransmission();
  
   // Check if device has been connected or removed
  if((SS1_CONNECTED && !SS1_LAST) || (!SS1_CONNECTED && SS1_LAST)) {
    NEW_DEVICE = SS1_ADDRESS;
    interruptRPi();
  }
  
  if((SS2_CONNECTED && !SS2_LAST) || (!SS2_CONNECTED && SS2_LAST)) {
    NEW_DEVICE = SS2_ADDRESS;
    interruptRPi();
  }
  
  
}



