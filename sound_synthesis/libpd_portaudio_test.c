/** @file paex_sine.c
    @ingroup examples_src
    @brief Play a sine wave for several seconds.
    @author Ross Bencina <rossb@audiomulch.com>
    @author Phil Burk <philburk@softsynth.com>
*/
/*
 * $Id: paex_sine.c 1752 2011-09-08 03:21:55Z philburk $
 *
 * This program uses the PortAudio Portable Audio Library.
 * For more information see: http://www.portaudio.com/
 * Copyright (c) 1999-2000 Ross Bencina and Phil Burk
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * The text above constitutes the entire PortAudio license; however,
 * the PortAudio community also makes the following non-binding requests:
 *
 * Any person wishing to distribute modifications to the Software is
 * requested to send the modifications to the original developer so that
 * they can be incorporated into the canonical version. It is also
 * requested that these non-binding requests be included along with the
 * license above.
 */
#include <stdio.h>
#include <math.h>
#include "portaudio.h"
#include "z_libpd.h"

#define NUM_SECONDS   (5)
#define SAMPLE_RATE   (44100)
#define FRAMES_PER_BUFFER  (1024)

#ifndef M_PI
#define M_PI  (3.14159265)
#endif

#define TABLE_SIZE   (800)
typedef struct
{
    float sine[TABLE_SIZE];
    int left_phase;
    int right_phase;
    char message[20];
}
paTestData;

/* This routine will be called by the PortAudio engine when audio is needed.
** It may called at interrupt level on some machines so don't do anything
** that could mess up the system like calling malloc() or free().
*/
static int patestCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData )
{
    paTestData *data = (paTestData*)userData;
    float *out = (float*)outputBuffer;
    unsigned long i;

    (void) timeInfo; /* Prevent unused variable warnings. */
    (void) statusFlags;
    (void) inputBuffer;

    float inbuf[64], outbuf[128];  // one input channel, two output channels

    for ( i = 0; i < framesPerBuffer / 64; i++ ) {
        // generate new ticks every 128 bytes (2x)
        libpd_process_float(1, inbuf, outbuf);

        int j = 0;
        for (j = 0; j < 64 * 2; j += 2) {
            *out++ = (float)outbuf[j];  /* left */
            *out++ = (float)outbuf[j + 1]; /* right */
        }
    }

    return paContinue;
}

/*
 * This routine is called by portaudio when playback is done.
 */
static void StreamFinished( void* userData )
{
    paTestData *data = (paTestData *) userData;
    printf( "Stream Completed: %s\n", data->message );
}

/*******************************************************************/
int main(int argc, char **argv)
{
    PaStreamParameters outputParameters;
    PaStream *stream;
    PaError err;
    paTestData data;
    int i;

    // init pd
    libpd_init();
    libpd_init_audio(1, 2, SAMPLE_RATE);
    // block size 64, one tick per buffer

    // compute audio    [; pd dsp 1(
    //libpd_finish_message("pd", "dsp");
    // open patch       [; pd open file folder(
    libpd_openfile(argv[1], argv[2]);

    printf("PortAudio Test: output sine wave. SR = %d, BufSize = %d\n", SAMPLE_RATE, FRAMES_PER_BUFFER);

    err = Pa_Initialize();
    if ( err != paNoError ) goto error;

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) {
        fprintf(stderr, "Error: No default output device.\n");
        goto error;
    }
    outputParameters.channelCount = 2;       /* stereo output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    err = Pa_OpenStream(
              &stream,
              NULL, /* no input */
              &outputParameters,
              SAMPLE_RATE,
              FRAMES_PER_BUFFER,
              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
              patestCallback,
              &data );
    if ( err != paNoError ) goto error;

    sprintf( data.message, "No Message" );
    err = Pa_SetStreamFinishedCallback( stream, &StreamFinished );
    if ( err != paNoError ) goto error;

    err = Pa_StartStream( stream );
    if ( err != paNoError ) goto error;

    libpd_float("b0", 1);
    libpd_float("b1", 1);
    libpd_float("b2", 1);
    libpd_float("b3", 1);
    libpd_float("b4", 1);
    libpd_float("b5", 1);
    libpd_float("b6", 1);
    libpd_float("b7", 1);
    libpd_float("b8", 1);
    libpd_float("b9", 1);
    libpd_float("b10", 1);
    libpd_float("b11", 1);
    libpd_float("b12", 1);
    libpd_float("b13", 1);
    libpd_float("b14", 1);
    libpd_float("b15", 1);

    while (1) {
        int b = libpd_float("benni", 1);
        printf("RETURNED: %i\n", b);
        printf("Play for %d seconds.\n", NUM_SECONDS * 1000);
        Pa_Sleep( NUM_SECONDS * 1000 );
        libpd_float("b0", 0);
        libpd_float("b1", 0);
        libpd_float("b2", 0);
        libpd_float("b3", 0);
        libpd_float("b4", 0);
        libpd_float("b5", 0);
        libpd_float("b6", 0);
        libpd_float("b7", 0);
        libpd_float("b8", 0);
        libpd_float("b9", 0);
        libpd_float("b10", 0);
        libpd_float("b11", 0);
        libpd_float("b12", 0);
        libpd_float("b13", 0);
        libpd_float("b14", 0);
        libpd_float("b15", 0);        
        Pa_Sleep( NUM_SECONDS * 1000 );
        libpd_float("b0", 1);
        libpd_float("b1", 1);
        libpd_float("b2", 1);
        libpd_float("b3", 1);
        libpd_float("b4", 1);
        libpd_float("b5", 1);
        libpd_float("b6", 1);
        libpd_float("b7", 1);
        libpd_float("b8", 1);
        libpd_float("b9", 1);
        libpd_float("b10", 1);
        libpd_float("b11", 1);
        libpd_float("b12", 1);
        libpd_float("b13", 1);
        libpd_float("b14", 1);
        libpd_float("b15", 1);
    }


    err = Pa_StopStream( stream );
    if ( err != paNoError ) goto error;

    err = Pa_CloseStream( stream );
    if ( err != paNoError ) goto error;

    Pa_Terminate();
    printf("Test finished.\n");

    return err;
error:
    Pa_Terminate();
    fprintf( stderr, "An error occured while using the portaudio stream\n" );
    fprintf( stderr, "Error number: %d\n", err );
    fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( err ) );
    return err;
}
