## Sound synthesis part of MISA
The required libraries are:

- [libpd](https://github.com/libpd)
- [portaudio](http://www.portaudio.com/)

## Installation
First you must install libpd. You can do that by cloning the libpd git repo and following the instructions [here](https://github.com/libpd).

Next you must install portaudio

Finally you must configure the Makefile to find the files you have compiled. Good luck.